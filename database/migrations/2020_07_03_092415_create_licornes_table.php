<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicornesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licornes', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->text('description');
            $table->bigInteger('price');
            $table->text('reproduction_date');
            $table->bigInteger('elevage_id')->unsigned()->nullable();
            $table->foreign('elevage_id')->references('id')->on('elevages')->onDelete('SET NULL');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licornes');
    }
}
