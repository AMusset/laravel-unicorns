@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Licornes</div>

                <div class="card-body">

                    <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                    <br>
                    @if (Auth::user()->role=='eleveur' or Auth::user()->role=='reproducteur')
                    <a href="{{ route('licornes.create') }}" class="btn btn-success" title="Ajouter une licorne">Ajouter une licorne</a>
                    @endif
                    
                    <br>

                    <ul>
                        @foreach($licornes as $licorne)
                            <li>
                                <a href="{{ route('licornes.show', $licorne->id) }}" title="{{ $licorne->name }}">{{ $licorne->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
