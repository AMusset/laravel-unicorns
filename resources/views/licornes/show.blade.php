@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Affichage de licorne</div>

                <div class="card-body">

                    <a class="btn btn-success" href="{{ route('licornes.index') }}">Retour a la liste</a>

                    <a class="btn btn-warning" href="{{ route('licornes.edit', $licorne->id) }}">Modifier</a>

                    <form action="{{ route('licornes.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $licorne->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>

                    <p>Nom : {{ $licorne->name }}</p>
                    <p>Description : {{ $licorne->description }}</p>
                    <p>Prix : {{ $licorne->price }}</p>
                    <p>Date de reproduction : {{ $licorne->reproduction_date }}</p>

                    <br>

                    @if(!is_null($licorne->user))
                        <p>
                            Cette licorne appartient à l'utilisateur : <a href="{{ route('users.show', $licorne->user->id) }}">{{ $licorne->user->name }}</a>
                        </p>
                    @endif

                    @if(!is_null($licorne->user))
                        <p>
                            Cette licorne provient de l'élevage : <a href="{{ route('elevages.show', $licorne->elevage->id) }}">{{ $licorne->elevage->name }}</a>
                        </p>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
