@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Affichage de l'élevage</div>

                <div class="card-body">

                    <a class="btn btn-success" href="{{ route('elevages.index') }}">Retour a la liste</a>

                    <a class="btn btn-warning" href="{{ route('elevages.edit', $elevage->id) }}">Modifier</a>

                    <form action="{{ route('elevages.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $elevage->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>

                    <p>Nom : {{ $elevage->name }}</p>
                    <p>Localisation : {{ $elevage->place }}</p>
                    <p>Population : {{ $elevage->population }}</p>

                    <br>

                    @if(!is_null($elevage->eleveur))
                        <p>
                            Cet élevage appartient à l'éleveur : <a href="{{ route('eleveurs.show', $elevage->eleveur->id) }}">{{ $elevage->eleveur->name }}</a>
                        </p>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
