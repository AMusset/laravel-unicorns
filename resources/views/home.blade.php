@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(Auth::user()->role=='eleveur')
                    <a href="{{ route('elevages.index') }}" class="btn btn-success" title="Gérer les elevages">Gérer les élevages</a>
                    @else
                    <a href="{{ route('elevages.index') }}" class="btn btn-success" title="Voir les elevages">Voir les élevages</a>
                    @endif

                    @if(Auth::user()->role=='eleveur' or Auth::user()->role=='reproducteur')
                    <a href="{{ route('licornes.index') }}" class="btn btn-success" title="Gérer les licornes">Gérer les licornes</a>
                    @else
                    <a href="{{ route('licornes.index') }}" class="btn btn-success" title="Voir les licornes">Voir les licornes</a>
                    @endif

                    <a href="{{ route('users.index') }}" class="btn btn-success" title="Voir les gerants">Voir les gérants</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
