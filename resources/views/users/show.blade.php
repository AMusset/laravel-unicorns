@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $user->name }}</div>

                <div class="card-body">

                    <a class="btn btn-success" href="{{ route('users.index') }}">Retour a la liste</a>

                    @if($user->role=='eleveur')
                    <p>Elevages :</p>
                        @foreach($elevages as $elevage)
                            @if($elevage->eleveur->id=$user->id)
                                <li>
                                    <a href="{{ route('elevages.show', $elevage->id) }}" title="{{ $elevage->name }}">{{ $elevage->name }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif

                    <p>Licornes :</p>
                        @foreach($licornes as $licorne)
                            @if($licorne->user->id==$user->id)
                                <li>
                                    <a href="{{ route('licornes.show', $licorne->id) }}" title="{{ $licorne->name }}">{{ $licorne->name }}</a>
                                </li>
                            @endif
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
