@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Gérants</div>

                <div class="card-body">

                    <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                    <br>

                    <ul>
                        @foreach($users as $user)
                            @if($user->role!='acheteur')
                                <li>
                                    <a href="{{ route('users.show', $user->id) }}" title="{{ $user->name }}">{{ $user->role }} : {{ $user->name }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
