<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eleveur extends Model
{
    protected $table = "eleveurs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    ];
    
}
