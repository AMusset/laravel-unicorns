<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reproducteur extends Model
{
    protected $table = "reproducteurs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    ];

}
