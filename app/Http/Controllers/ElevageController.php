<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Elevage;
use App\Eleveur;
use Auth;

class ElevageController extends Controller
{
    public function create()
    {
        $eleveurs = Eleveur::all();
        return view('elevages.create', compact('eleveurs'));
    }

    public function store(Request $request)
    {
        $elevage = new Elevage();
        $elevage->name = $request->get('name');
        $elevage->place = $request->get('place');
        $elevage->population = $request->get('population');
        $elevage->description = $request->get('description');
        // Trouver l'eleveur en fonction du user id
        $elevage->eleveur_id = Auth::user()->id;
        $elevage->save();
        
        return redirect()->route('elevages.index');
    }

    public function show($id)
    {
        $elevage = Elevage::with('eleveur')->find($id);

        return view('elevages.show', compact('elevage'));
    }

    public function index()
    {
        $elevages = Elevage::all();

        return view('elevages.index', compact('elevages'));
    }

    public function edit($id)
    {
        $elevage = Elevage::find($id);

        return view('elevages.edit', compact('elevage'));
    }

    public function update(Request $request, $id)
    {
        $elevage = Elevage::find($id);
        $elevage->name = $request->get('name');
        $elevage->place = $request->get('place');
        $elevage->population = $request->get('population');
        $elevage->description = $request->get('description');
        $elevage->save();
        
        return redirect()->route('elevages.index');
    }

    public function destroy(Request $request)
    {
        $elevage = Elevage::find($request->get('id'));
        $elevage->delete();

        return redirect()->route('elevages.index');
    }
}
