<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Licorne;
use App\Elevage;
use App\User;

class LicorneController extends Controller
{
    public function create()
    {
        $elevages = Elevage::all();
        return view('licornes.create', compact('elevages'));
    }

    public function store(Request $request)
    {
        $licorne = new Licorne();
        $licorne->name = $request->get('name');
        $licorne->description = $request->get('description');
        $licorne->price = $request->get('price');
        $licorne->reproduction_date = $request->get('reproduction_date');
        $licorne->elevage_id = $request->get('elevage_id');
        $licorne->user_id = $request->get('user_id');
        $licorne->save();
        
        return redirect()->route('licornes.index');
    }

    public function show($id)
    {
        $licorne = Licorne::with('elevage')->find($id);

        return view('licornes.show', compact('licorne'));
    }

    public function index()
    {
        $licornes = Licorne::all();

        return view('licornes.index', compact('licornes'));
    }

    public function edit($id)
    {
        $licorne = Licorne::find($id);

        return view('licornes.edit', compact('licorne'));
    }

    public function update(Request $request, $id)
    {
        $licorne = Licorne::find($id);
        $licorne->name = $request->get('name');
        $licorne->description = $request->get('description');
        $licorne->price = $request->get('price');
        $licorne->reproduction_date = $request->get('reproduction_date');
        $licorne->elevage_id = $request->get('elevage_id');
        $licorne->user_id = $request->get('user_id');
        $licorne->save();
        
        return redirect()->route('licornes.index');
    }

    public function destroy(Request $request)
    {
        $licorne = Licorne::find($request->get('id'));
        $licorne->delete();

        return redirect()->route('licornes.index');
    }
}
