<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elevage extends Model
{
	protected $table = "elevages";

	protected $fillable = [
        "id", "name", "place", "population", "description", "eleveur_id"
    ];

    public function eleveur()
    {
        return $this->belongsTo(Eleveur::class, 'eleveur_id');
    }
}
