<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licorne extends Model
{
	protected $table = "licornes";

	protected $fillable = [
        "id", "name", "description", "price", "reproduction_date", "elevage_id", "user_id"
    ];

    public function elevage()
    {
        return $this->belongsTo(Elevage::class, 'elevage_id');
    }

    public function user()
    {
        return $this->belongsTo(Category::class, 'user_id');
    }
}
