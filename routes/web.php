<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/licornes', 'LicorneController@index')->name('licornes.index');
Route::get('/licornes/{id}/show', 'LicorneController@show')->name('licornes.show');
Route::get('/licornes/create', 'LicorneController@create')->name('licornes.create');
Route::get('/licornes/{id}/edit', 'LicorneController@edit')->name('licornes.edit');
Route::post('/licornes', 'LicorneController@store')->name('licornes.store');
Route::put('/licornes/{id}', 'LicorneController@update')->name('licornes.update');
Route::delete('/licornes', 'LicorneController@destroy')->name('licornes.destroy');

Route::get('/elevages', 'ElevageController@index')->name('elevages.index');
Route::get('/elevages/{id}/show', 'ElevageController@show')->name('elevages.show');
Route::get('/elevages/create', 'ElevageController@create')->name('elevages.create');
Route::get('/elevages/{id}/edit', 'ElevageController@edit')->name('elevages.edit');
Route::post('/elevages', 'ElevageController@store')->name('elevages.store');
Route::put('/elevages/{id}', 'ElevageController@update')->name('elevages.update');
Route::delete('/elevages', 'ElevageController@destroy')->name('elevages.destroy');

Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/{id}/show', 'UserController@show')->name('users.show');